#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{

	Mat frame(Size(640, 420),CV_8UC3);
    Mat frame2(Size(640, 420),CV_8UC3);
    Mat hsv(Size(640, 420),CV_8UC3);
    Mat bw(Size(640, 420),CV_8UC1);
    Mat Erode(Size(640, 420),CV_8UC1);
	Mat Dialate(Size(640, 420),CV_8UC1);

	vector<Vec4i> hierarchy;
	vector<Point>::const_iterator d;

	vector<vector<Point> > contours_hull;
	vector<vector<Point> > hull;
	vector<vector<Point> > hullsI;
	vector<vector<Point> > defects;
	vector<vector<Point> > contour;
	vector<vector<Point> > contours_poly;

	RotatedRect* minRect;
	Rect* boundRect;

	int i=0;

	Moments moment;

	VideoCapture cap(0);
    if ( !cap.isOpened() )
    {
         cout << "Cannot open the web cam" << endl;
         return -1;
    }

    namedWindow("Control", CV_WINDOW_AUTOSIZE); 
  	int iLowH = 62;
 	int iHighH = 123;

  	int iLowS = 62; 
 	int iHighS = 255;

  	int iLowV = 0;
 	int iHighV = 255;

 	cap.read(frame);

 	contours_hull.resize(3);
	cvtColor(frame,hsv,CV_RGB2HSV);
	inRange(hsv,Scalar(66, 16, 66), Scalar(255, 255, 128),bw);

	findContours(bw,contours_hull,hierarchy,CV_RETR_CCOMP,CV_CHAIN_APPROX_SIMPLE);
	cout << contours_hull.size() << "\n";

	int j, IndexOfBiggestContour, maxarea=0, area,x ,y;
            for(j=0; j< contours_hull.size(); j++)
            {
                moment = moments((Mat)contours_hull.at(j));
                area= moment.m00;
                if(area>maxarea)
                {
                    maxarea = area;
                    IndexOfBiggestContour = j;
                }
            }
            cout << IndexOfBiggestContour << "\n";
	for( i = 0; i < contours_hull.size(); i++ )
     { 
          convexHull( contours_hull[i], hull[i], false, true );
          convexHull( contours_hull[i], hullsI[i], false, true );
            if(IndexOfBiggestContour == i)
               {
                  minRect[i] = minAreaRect( Mat(contours_hull[i]) );
                  
                  drawContours( frame, contours_hull,IndexOfBiggestContour, CV_RGB(255,255,255), 2, 8, hierarchy,0, Point() );
                  drawContours( frame, hull, IndexOfBiggestContour, CV_RGB(255,0,0), 2, 8, hierarchy, 0, Point() );

                 
                  approxPolyDP( Mat(contours_hull[i]), contours_poly[i], 3, true );
                  boundRect[i] = boundingRect( Mat(contours_poly[i]) );
                
                  rectangle( frame2, boundRect[i].tl(), boundRect[i].br(), CV_RGB(0,0,0), 2, 8, 0 );
                  Point2f rect_points[4];
                  minRect[i].points( rect_points );
                   for( int j = 0; j < 4; j++ )
                       {
                         line( frame2, rect_points[j], rect_points[(j+1)%4], CV_RGB(255,255,0), 2, 8 );
                       }
             }
     }
return 0;
}